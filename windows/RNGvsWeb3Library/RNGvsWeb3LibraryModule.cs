using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Gvs.Web3.Library.RNGvsWeb3Library
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNGvsWeb3LibraryModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNGvsWeb3LibraryModule"/>.
        /// </summary>
        internal RNGvsWeb3LibraryModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNGvsWeb3Library";
            }
        }
    }
}
