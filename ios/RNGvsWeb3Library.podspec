
Pod::Spec.new do |s|
  s.name         = "RNGvsWeb3Library"
  s.version      = "1.0.0"
  s.summary      = "RNGvsWeb3Library"
  s.description  = <<-DESC
                  RNGvsWeb3Library
                   DESC
  s.homepage     = ""
  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "author" => "author@domain.cn" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://github.com/author/RNGvsWeb3Library.git", :tag => "master" }
  s.source_files  = "RNGvsWeb3Library/**/*.{h,m}"
  s.requires_arc = true


  s.dependency "React"
  #s.dependency "others"

end

  