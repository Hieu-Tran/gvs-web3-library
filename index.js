
import { NativeModules } from 'react-native';
import './global';

const { RNGvsWeb3Library } = NativeModules;
const Web3 = require('web3');

export {Web3};
export default RNGvsWeb3Library;
